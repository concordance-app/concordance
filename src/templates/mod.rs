use std::collections::HashMap;
use serde::{Serialize,Deserialize};
use std::path::PathBuf;
use std::fs::{OpenOptions,File};
use std::io::prelude::*;
use uuid::{uuid,Uuid};
use crate::items;

#[derive(Debug, Serialize, Deserialize)]
pub struct Template {
    id: Uuid,
    keys: HashMap<items::ItemKey, Box<dyn items::ItemValue>>,
    saved: bool
}

impl Template {
    pub fn new () -> Template {
        Template { id: Uuid::new_v4(), keys: HashMap::new(), saved: false }
    }

    pub fn id (&self) -> Uuid {
        self.id
    }

    pub fn insert (&mut self, K: items::ItemKey, V: Box<dyn items::ItemValue>) -> Result<(), &'static str> {
        self.saved = false;
        self.keys.insert(K, V);
        Ok(())
    }

    fn to_json (&self) -> Result<String, &'static str> {
        Ok(serde_json::to_string(self).unwrap())
    }

    fn from_json (json: &str) -> Result<Template, &'static str> {
        let result: Template = serde_json::from_str(json).unwrap();
        Ok(result)
    }

    pub fn store (&mut self, dir: &PathBuf) -> Result<PathBuf, &'static str> {
        self.saved = true;
        let json = self.to_json()?;

        let mut path: PathBuf = dir.clone();
        path.push(PathBuf::from(self.id.to_string()));
        path.set_extension("json");

        let mut file = OpenOptions::new().write(true).create(true).open(&path).unwrap();
        file.write_all(json.as_bytes()).unwrap();

        Ok(path)
    }

    pub fn unstore (path: &PathBuf) -> Result<Template, &'static str> {
        let mut file = OpenOptions::new().read(true).open(path).unwrap();

        let mut json = String::new();
        file.read_to_string(&mut json).unwrap();

        let result = Template::from_json(json.as_str()).unwrap();
        Ok(result)
    }
}