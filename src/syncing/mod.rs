use std::path::PathBuf;
use std::fs::{self,DirBuilder};
use crate::templates::Template;
use std::collections::HashMap;
use uuid::{uuid, Uuid};

#[derive(Debug)]
pub struct TemplateManager {
    templates: HashMap<Uuid, Template>,
    config_dir: PathBuf,
    templates_dir: PathBuf
}

const TEMPLATES_DIR_NAME: &str = "templates";

impl TemplateManager {
    pub fn new (config_dir: &PathBuf) -> TemplateManager {
        let config_dir: PathBuf = PathBuf::from(config_dir);
        let templates_dir: PathBuf = config_dir.clone().join(TEMPLATES_DIR_NAME);
        if !templates_dir.exists() {
            DirBuilder::new().create(templates_dir.clone()).unwrap();
        }
        TemplateManager { templates: HashMap::new(), config_dir, templates_dir }
    }

    pub fn update (&mut self) -> Result<(), &'static str> {
        let dirs = fs::read_dir(&self.templates_dir).unwrap();
        for dir in dirs {
            let dir = dir.unwrap().path();
            let id: Uuid = Uuid::parse_str(&dir.file_stem().unwrap().to_str().unwrap()).unwrap();
            if !self.templates.contains_key(&id) {
                self.templates.insert(id.clone(), Template::unstore(&dir).unwrap());
            }
        }
        Ok(())
    }

    pub fn templates (&mut self) -> &mut HashMap<Uuid, Template> {
        &mut self.templates
    }

    pub fn template (&mut self, id: &Uuid) -> Result<&mut Template, &'static str> {
        match self.templates.get_mut(id) {
            Some (t) => Ok(t),
            None => Err("TODO Placeholder Error"),
        }
    }

    pub fn create_template (&mut self) -> Result<(), &'static str> {
        let template = Template::new();
        let id = template.id();
        self.templates.insert(id, template);
        let template = self.templates.get_mut(&id).unwrap();
        template.store(&self.templates_dir)?;
        Ok(())
    }
}