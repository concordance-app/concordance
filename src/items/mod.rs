use serde::{Serialize,Deserialize};
use serde::de::{self, Deserializer, Visitor, SeqAccess, MapAccess};
use std::collections::HashMap;
use std::fmt;

pub mod packages;

#[derive(Debug,Serialize,Deserialize,Hash,Eq,PartialEq)]
pub enum ItemKey {
    DConf(String),
    PackageGroup(packages::PackageManager),
}


pub trait ItemValue {

}

impl fmt::Debug for dyn ItemValue {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "This should not show up")
    }
}

impl Serialize for dyn ItemValue {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: serde::Serializer {
                serializer.serialize_str("This is an error")
    }
}

impl<'de> Deserialize<'de> for Box<dyn ItemValue> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        enum Field { Secs, Nanos }

        // This part could also be generated independently by:
        //
        //    #[derive(Deserialize)]
        //    #[serde(field_identifier, rename_all = "lowercase")]
        //    enum Field { Secs, Nanos }
        impl<'de> Deserialize<'de> for Field {
            fn deserialize<D>(deserializer: D) -> Result<Field, D::Error>
            where
                D: Deserializer<'de>,
            {
                struct FieldVisitor;

                impl<'de> Visitor<'de> for FieldVisitor {
                    type Value = Field;

                    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                        formatter.write_str("`secs` or `nanos`")
                    }

                    fn visit_str<E>(self, value: &str) -> Result<Field, E>
                    where
                        E: de::Error,
                    {
                        match value {
                            _ => Err(de::Error::unknown_field(value, FIELDS)),
                        }
                    }
                }

                deserializer.deserialize_identifier(FieldVisitor)
            }
        }

        struct ItemValueVisitor;

        impl<'de> Visitor<'de> for ItemValueVisitor {
            type Value = Box<dyn ItemValue>;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct TestData")
            }

            fn visit_seq<V>(self, mut seq: V) -> Result<Box<dyn ItemValue>, V::Error>
            where
                V: SeqAccess<'de>,
            {
                Ok(Box::new(TestItemValue {}))
            }

            fn visit_map<V>(self, mut map: V) -> Result<Box<dyn ItemValue>, V::Error>
            where
                V: MapAccess<'de>,
            {
                Ok(Box::new(TestItemValue {}))
            }
        }

        const FIELDS: &'static [&'static str] = &["secs", "nanos"];
        deserializer.deserialize_struct("Duration", FIELDS, ItemValueVisitor)
    }
}

#[derive(Debug,Serialize,Deserialize,Hash,Eq,PartialEq)]
pub struct PackageList {

}

impl ItemValue for PackageList {

}

pub struct TestItemValue {

}

impl ItemValue for TestItemValue {

}