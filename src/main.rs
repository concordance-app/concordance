pub mod templates;
pub mod syncing;
pub mod items;

use std::path::PathBuf;
use std::fs::DirBuilder;

fn main() {
    const TEST_DIRS: &str = "test_dirs";
    const CONFIG_DIR: &str = "test_dirs/config";

    let mut template_manager = syncing::TemplateManager::new(&PathBuf::from(CONFIG_DIR.clone()));
    template_manager.create_template();
    template_manager.update();
    println!("{:#?}", template_manager.templates());
}
